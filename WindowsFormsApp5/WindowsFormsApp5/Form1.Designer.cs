﻿namespace WindowsFormsApp5
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_start = new System.Windows.Forms.Button();
            this.btn_kraj = new System.Windows.Forms.Button();
            this.lbl_slovo = new System.Windows.Forms.Label();
            this.lbl_rijec = new System.Windows.Forms.Label();
            this.txt_slovo = new System.Windows.Forms.TextBox();
            this.txt_rijec = new System.Windows.Forms.TextBox();
            this.lbl_preostalo = new System.Windows.Forms.Label();
            this.txt_preostali = new System.Windows.Forms.TextBox();
            this.txt_rjesenje = new System.Windows.Forms.TextBox();
            this.lbl_rjesenje = new System.Windows.Forms.Label();
            this.btn_provjera_slovo = new System.Windows.Forms.Button();
            this.btn_provjera_rijec = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_start
            // 
            this.btn_start.Location = new System.Drawing.Point(13, 13);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(75, 23);
            this.btn_start.TabIndex = 0;
            this.btn_start.Text = "pokreni";
            this.btn_start.UseVisualStyleBackColor = true;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // btn_kraj
            // 
            this.btn_kraj.Location = new System.Drawing.Point(262, 226);
            this.btn_kraj.Name = "btn_kraj";
            this.btn_kraj.Size = new System.Drawing.Size(75, 23);
            this.btn_kraj.TabIndex = 1;
            this.btn_kraj.Text = "izlaz";
            this.btn_kraj.UseVisualStyleBackColor = true;
            this.btn_kraj.Click += new System.EventHandler(this.btn_kraj_Click);
            // 
            // lbl_slovo
            // 
            this.lbl_slovo.AutoSize = true;
            this.lbl_slovo.Location = new System.Drawing.Point(10, 71);
            this.lbl_slovo.Name = "lbl_slovo";
            this.lbl_slovo.Size = new System.Drawing.Size(63, 13);
            this.lbl_slovo.TabIndex = 2;
            this.lbl_slovo.Text = "unesi slovo:";
            this.lbl_slovo.Click += new System.EventHandler(this.lbl_slovo_Click);
            // 
            // lbl_rijec
            // 
            this.lbl_rijec.AutoSize = true;
            this.lbl_rijec.Location = new System.Drawing.Point(13, 102);
            this.lbl_rijec.Name = "lbl_rijec";
            this.lbl_rijec.Size = new System.Drawing.Size(57, 13);
            this.lbl_rijec.TabIndex = 3;
            this.lbl_rijec.Text = "unesi rijec:";
            this.lbl_rijec.Click += new System.EventHandler(this.lbl_rijec_Click);
            // 
            // txt_slovo
            // 
            this.txt_slovo.Location = new System.Drawing.Point(79, 64);
            this.txt_slovo.Name = "txt_slovo";
            this.txt_slovo.Size = new System.Drawing.Size(100, 20);
            this.txt_slovo.TabIndex = 4;
            this.txt_slovo.TextChanged += new System.EventHandler(this.txt_slovo_TextChanged);
            // 
            // txt_rijec
            // 
            this.txt_rijec.Location = new System.Drawing.Point(79, 95);
            this.txt_rijec.Name = "txt_rijec";
            this.txt_rijec.Size = new System.Drawing.Size(100, 20);
            this.txt_rijec.TabIndex = 5;
            this.txt_rijec.TextChanged += new System.EventHandler(this.txt_rijec_TextChanged);
            // 
            // lbl_preostalo
            // 
            this.lbl_preostalo.AutoSize = true;
            this.lbl_preostalo.Location = new System.Drawing.Point(10, 134);
            this.lbl_preostalo.Name = "lbl_preostalo";
            this.lbl_preostalo.Size = new System.Drawing.Size(112, 13);
            this.lbl_preostalo.TabIndex = 6;
            this.lbl_preostalo.Text = "preostali broj pokusaja";
            this.lbl_preostalo.Click += new System.EventHandler(this.lbl_preostalo_Click);
            // 
            // txt_preostali
            // 
            this.txt_preostali.Location = new System.Drawing.Point(128, 127);
            this.txt_preostali.Name = "txt_preostali";
            this.txt_preostali.Size = new System.Drawing.Size(100, 20);
            this.txt_preostali.TabIndex = 7;
            this.txt_preostali.TextChanged += new System.EventHandler(this.txt_preostali_TextChanged);
            // 
            // txt_rjesenje
            // 
            this.txt_rjesenje.Location = new System.Drawing.Point(86, 165);
            this.txt_rjesenje.Name = "txt_rjesenje";
            this.txt_rjesenje.Size = new System.Drawing.Size(100, 20);
            this.txt_rjesenje.TabIndex = 8;
            this.txt_rjesenje.TextChanged += new System.EventHandler(this.txt_rjesenje_TextChanged);
            // 
            // lbl_rjesenje
            // 
            this.lbl_rjesenje.AutoSize = true;
            this.lbl_rjesenje.Location = new System.Drawing.Point(13, 172);
            this.lbl_rjesenje.Name = "lbl_rjesenje";
            this.lbl_rjesenje.Size = new System.Drawing.Size(67, 13);
            this.lbl_rjesenje.TabIndex = 9;
            this.lbl_rjesenje.Text = "trazena rijec:";
            this.lbl_rjesenje.Click += new System.EventHandler(this.lbl_rjesenje_Click);
            // 
            // btn_provjera_slovo
            // 
            this.btn_provjera_slovo.Location = new System.Drawing.Point(185, 61);
            this.btn_provjera_slovo.Name = "btn_provjera_slovo";
            this.btn_provjera_slovo.Size = new System.Drawing.Size(127, 23);
            this.btn_provjera_slovo.TabIndex = 10;
            this.btn_provjera_slovo.Text = "provjeri za slovo";
            this.btn_provjera_slovo.UseVisualStyleBackColor = true;
            this.btn_provjera_slovo.Click += new System.EventHandler(this.btn_zaslovo_Click);
            // 
            // btn_provjera_rijec
            // 
            this.btn_provjera_rijec.Location = new System.Drawing.Point(186, 95);
            this.btn_provjera_rijec.Name = "btn_provjera_rijec";
            this.btn_provjera_rijec.Size = new System.Drawing.Size(126, 23);
            this.btn_provjera_rijec.TabIndex = 11;
            this.btn_provjera_rijec.Text = "provjeri za rijec";
            this.btn_provjera_rijec.UseVisualStyleBackColor = true;
            this.btn_provjera_rijec.Click += new System.EventHandler(this.btn_provjera_rijec_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(349, 261);
            this.Controls.Add(this.btn_provjera_rijec);
            this.Controls.Add(this.btn_provjera_slovo);
            this.Controls.Add(this.lbl_rjesenje);
            this.Controls.Add(this.txt_rjesenje);
            this.Controls.Add(this.txt_preostali);
            this.Controls.Add(this.lbl_preostalo);
            this.Controls.Add(this.txt_rijec);
            this.Controls.Add(this.txt_slovo);
            this.Controls.Add(this.lbl_rijec);
            this.Controls.Add(this.lbl_slovo);
            this.Controls.Add(this.btn_kraj);
            this.Controls.Add(this.btn_start);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.Button btn_kraj;
        private System.Windows.Forms.Label lbl_slovo;
        private System.Windows.Forms.Label lbl_rijec;
        private System.Windows.Forms.TextBox txt_slovo;
        private System.Windows.Forms.TextBox txt_rijec;
        private System.Windows.Forms.Label lbl_preostalo;
        private System.Windows.Forms.TextBox txt_preostali;
        private System.Windows.Forms.TextBox txt_rjesenje;
        private System.Windows.Forms.Label lbl_rjesenje;
        private System.Windows.Forms.Button btn_provjera_slovo;
        private System.Windows.Forms.Button btn_provjera_rijec;
    }
}

