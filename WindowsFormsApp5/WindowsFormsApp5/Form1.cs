﻿//Napravite jednostavnu igru vješala.Pojmovi se učitavaju u listu iz datoteke, i u
//svakoj partiji se odabire nasumični pojam iz liste. Omogućiti svu
//funkcionalnost koju biste očekivali od takve igre.Nije nužno crtati vješala,
//dovoljno je na labeli ispisati koliko je pokušaja za odabir slova preostalo.


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp5
{
    public partial class Form1 : Form
    {
        List<string> pojmovi = new List<string>();

        char slovo;
        int brojac = 0;
        string rijec;
        char[] stvorenarijec;
        int brpokusaja = 10;
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_start_Click(object sender, EventArgs e)
        {

            string path = "C:\\Users\\Antonija\\Desktop\\vjesalo.txt";
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    pojmovi.Add(line);
                }
            }
            Random random = new Random();
            rijec = pojmovi[random.Next(0, pojmovi.Count - 1)];
            int brojslova = rijec.Length;
            stvorenarijec = new char[brojslova];
            brpokusaja = 10;
            txt_preostali.Text = (brpokusaja).ToString();
            txt_rjesenje.Text = "?";
            txt_rijec.Text = " ";
            txt_slovo.Text = " ";

        }

        private void lbl_slovo_Click(object sender, EventArgs e)
        {

        }

        private void txt_slovo_TextChanged(object sender, EventArgs e)
        {

        }

        private void lbl_rijec_Click(object sender, EventArgs e)
        {

        }

        private void txt_rijec_TextChanged(object sender, EventArgs e)
        {

        }

        private void lbl_preostalo_Click(object sender, EventArgs e)
        {

        }

        private void txt_preostali_TextChanged(object sender, EventArgs e)
        {

        }

        private void lbl_rjesenje_Click(object sender, EventArgs e)
        {

        }

        private void txt_rjesenje_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_kraj_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_zaslovo_Click(object sender, EventArgs e)
        {
            int brojac = 0;
            if (!char.TryParse(txt_slovo.Text, out slovo))
            {
                MessageBox.Show("Pogrešan unos operanda slova!");
            }
            int brojslova = rijec.Length;
            for (int i = 0; i < brojslova; i++)
            {
                if ((slovo) == (rijec[i]))
                {
                    stvorenarijec[i] = slovo;
                    brojac++;
                }
            }
            if (brojac == 0)
            {
                brpokusaja--;
                txt_preostali.Text = (brpokusaja).ToString();
            }
            if (brpokusaja == 0)
            {
                MessageBox.Show("niste uspjeli pogoditi! Rijec je: (stisni ok) ");
                txt_rjesenje.Text = (rijec).ToString();
            }
        }

        private void btn_provjera_rijec_Click(object sender, EventArgs e)
        {
            
                bool jednakost = rijec.Equals(txt_rijec.Text);
                if (jednakost == true)
                {
                    MessageBox.Show("pogodak");
                    txt_rjesenje.Text = (rijec).ToString();
                }

            }
        }
    }
//kava
//voda
//ulica
//cesta
//sunce
//oblak

