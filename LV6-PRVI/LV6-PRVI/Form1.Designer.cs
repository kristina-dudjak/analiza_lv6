﻿namespace LV6_PRVI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_a = new System.Windows.Forms.Label();
            this.lbl_b = new System.Windows.Forms.Label();
            this.txt_a = new System.Windows.Forms.TextBox();
            this.txt_b = new System.Windows.Forms.TextBox();
            this.btn_zbroj = new System.Windows.Forms.Button();
            this.btn_oduzmi = new System.Windows.Forms.Button();
            this.btn_pomnozi = new System.Windows.Forms.Button();
            this.btn_podijeli = new System.Windows.Forms.Button();
            this.txt_zbroji = new System.Windows.Forms.TextBox();
            this.txt_oduzmi = new System.Windows.Forms.TextBox();
            this.txt_pomnozi = new System.Windows.Forms.TextBox();
            this.txt_podijeli = new System.Windows.Forms.TextBox();
            this.btn_sin_a = new System.Windows.Forms.Button();
            this.btn_cos_a = new System.Windows.Forms.Button();
            this.btn_log_a = new System.Windows.Forms.Button();
            this.btn_sqrt_a = new System.Windows.Forms.Button();
            this.btn_kvadrat_a = new System.Windows.Forms.Button();
            this.txt_sin_a = new System.Windows.Forms.TextBox();
            this.txt_cos_a = new System.Windows.Forms.TextBox();
            this.txt_log_a = new System.Windows.Forms.TextBox();
            this.txt_sqrt_a = new System.Windows.Forms.TextBox();
            this.txt_kvadrat_a = new System.Windows.Forms.TextBox();
            this.btn_sin_b = new System.Windows.Forms.Button();
            this.btn_cos_b = new System.Windows.Forms.Button();
            this.btn_log_b = new System.Windows.Forms.Button();
            this.btn_sqrt_b = new System.Windows.Forms.Button();
            this.btn_kvadrat_b = new System.Windows.Forms.Button();
            this.txt_sin_b = new System.Windows.Forms.TextBox();
            this.txt_cos_b = new System.Windows.Forms.TextBox();
            this.txt_log_b = new System.Windows.Forms.TextBox();
            this.txt_sqrt_b = new System.Windows.Forms.TextBox();
            this.txt_kvadrat_b = new System.Windows.Forms.TextBox();
            this.btn_izlaz = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_a
            // 
            this.lbl_a.AutoSize = true;
            this.lbl_a.Location = new System.Drawing.Point(13, 13);
            this.lbl_a.Name = "lbl_a";
            this.lbl_a.Size = new System.Drawing.Size(13, 13);
            this.lbl_a.TabIndex = 0;
            this.lbl_a.Text = "a";
            this.lbl_a.Click += new System.EventHandler(this.lbl_a_Click);
            // 
            // lbl_b
            // 
            this.lbl_b.AutoSize = true;
            this.lbl_b.Location = new System.Drawing.Point(13, 42);
            this.lbl_b.Name = "lbl_b";
            this.lbl_b.Size = new System.Drawing.Size(13, 13);
            this.lbl_b.TabIndex = 1;
            this.lbl_b.Text = "b";
            this.lbl_b.Click += new System.EventHandler(this.lbl_b_Click);
            // 
            // txt_a
            // 
            this.txt_a.Location = new System.Drawing.Point(33, 5);
            this.txt_a.Name = "txt_a";
            this.txt_a.Size = new System.Drawing.Size(100, 20);
            this.txt_a.TabIndex = 2;
            this.txt_a.TextChanged += new System.EventHandler(this.txt_a_TextChanged);
            // 
            // txt_b
            // 
            this.txt_b.Location = new System.Drawing.Point(33, 35);
            this.txt_b.Name = "txt_b";
            this.txt_b.Size = new System.Drawing.Size(100, 20);
            this.txt_b.TabIndex = 3;
            this.txt_b.TextChanged += new System.EventHandler(this.txt_b_TextChanged);
            // 
            // btn_zbroj
            // 
            this.btn_zbroj.Location = new System.Drawing.Point(16, 82);
            this.btn_zbroj.Name = "btn_zbroj";
            this.btn_zbroj.Size = new System.Drawing.Size(75, 23);
            this.btn_zbroj.TabIndex = 4;
            this.btn_zbroj.Text = "+";
            this.btn_zbroj.UseVisualStyleBackColor = true;
            this.btn_zbroj.Click += new System.EventHandler(this.btn_zbroj_Click);
            // 
            // btn_oduzmi
            // 
            this.btn_oduzmi.Location = new System.Drawing.Point(16, 121);
            this.btn_oduzmi.Name = "btn_oduzmi";
            this.btn_oduzmi.Size = new System.Drawing.Size(75, 23);
            this.btn_oduzmi.TabIndex = 5;
            this.btn_oduzmi.Text = "-";
            this.btn_oduzmi.UseVisualStyleBackColor = true;
            this.btn_oduzmi.Click += new System.EventHandler(this.btn_oduzmi_Click);
            // 
            // btn_pomnozi
            // 
            this.btn_pomnozi.Location = new System.Drawing.Point(16, 162);
            this.btn_pomnozi.Name = "btn_pomnozi";
            this.btn_pomnozi.Size = new System.Drawing.Size(75, 23);
            this.btn_pomnozi.TabIndex = 6;
            this.btn_pomnozi.Text = "*";
            this.btn_pomnozi.UseVisualStyleBackColor = true;
            this.btn_pomnozi.Click += new System.EventHandler(this.btn_pomnozi_Click);
            // 
            // btn_podijeli
            // 
            this.btn_podijeli.Location = new System.Drawing.Point(16, 202);
            this.btn_podijeli.Name = "btn_podijeli";
            this.btn_podijeli.Size = new System.Drawing.Size(75, 23);
            this.btn_podijeli.TabIndex = 7;
            this.btn_podijeli.Text = "/";
            this.btn_podijeli.UseVisualStyleBackColor = true;
            this.btn_podijeli.Click += new System.EventHandler(this.btn_podijeli_Click);
            // 
            // txt_zbroji
            // 
            this.txt_zbroji.Location = new System.Drawing.Point(98, 84);
            this.txt_zbroji.Name = "txt_zbroji";
            this.txt_zbroji.Size = new System.Drawing.Size(100, 20);
            this.txt_zbroji.TabIndex = 8;
            this.txt_zbroji.TextChanged += new System.EventHandler(this.txt_zbroji_TextChanged);
            // 
            // txt_oduzmi
            // 
            this.txt_oduzmi.Location = new System.Drawing.Point(98, 123);
            this.txt_oduzmi.Name = "txt_oduzmi";
            this.txt_oduzmi.Size = new System.Drawing.Size(100, 20);
            this.txt_oduzmi.TabIndex = 9;
            this.txt_oduzmi.TextChanged += new System.EventHandler(this.txt_oduzmi_TextChanged);
            // 
            // txt_pomnozi
            // 
            this.txt_pomnozi.Location = new System.Drawing.Point(98, 164);
            this.txt_pomnozi.Name = "txt_pomnozi";
            this.txt_pomnozi.Size = new System.Drawing.Size(100, 20);
            this.txt_pomnozi.TabIndex = 10;
            this.txt_pomnozi.TextChanged += new System.EventHandler(this.txt_pomnozi_TextChanged);
            // 
            // txt_podijeli
            // 
            this.txt_podijeli.Location = new System.Drawing.Point(98, 202);
            this.txt_podijeli.Name = "txt_podijeli";
            this.txt_podijeli.Size = new System.Drawing.Size(100, 20);
            this.txt_podijeli.TabIndex = 11;
            this.txt_podijeli.TextChanged += new System.EventHandler(this.txt_podijeli_TextChanged);
            // 
            // btn_sin_a
            // 
            this.btn_sin_a.Location = new System.Drawing.Point(236, 35);
            this.btn_sin_a.Name = "btn_sin_a";
            this.btn_sin_a.Size = new System.Drawing.Size(75, 23);
            this.btn_sin_a.TabIndex = 12;
            this.btn_sin_a.Text = "sin a:";
            this.btn_sin_a.UseVisualStyleBackColor = true;
            this.btn_sin_a.Click += new System.EventHandler(this.btn_sin_a_Click);
            // 
            // btn_cos_a
            // 
            this.btn_cos_a.Location = new System.Drawing.Point(236, 65);
            this.btn_cos_a.Name = "btn_cos_a";
            this.btn_cos_a.Size = new System.Drawing.Size(75, 23);
            this.btn_cos_a.TabIndex = 13;
            this.btn_cos_a.Text = "cos a:";
            this.btn_cos_a.UseVisualStyleBackColor = true;
            this.btn_cos_a.Click += new System.EventHandler(this.btn_cos_a_Click);
            // 
            // btn_log_a
            // 
            this.btn_log_a.Location = new System.Drawing.Point(236, 95);
            this.btn_log_a.Name = "btn_log_a";
            this.btn_log_a.Size = new System.Drawing.Size(75, 23);
            this.btn_log_a.TabIndex = 14;
            this.btn_log_a.Text = "log a:";
            this.btn_log_a.UseVisualStyleBackColor = true;
            this.btn_log_a.Click += new System.EventHandler(this.btn_log_a_Click);
            // 
            // btn_sqrt_a
            // 
            this.btn_sqrt_a.Location = new System.Drawing.Point(236, 124);
            this.btn_sqrt_a.Name = "btn_sqrt_a";
            this.btn_sqrt_a.Size = new System.Drawing.Size(75, 23);
            this.btn_sqrt_a.TabIndex = 15;
            this.btn_sqrt_a.Text = "sqrt a:";
            this.btn_sqrt_a.UseVisualStyleBackColor = true;
            this.btn_sqrt_a.Click += new System.EventHandler(this.btn_sqrt_a_Click);
            // 
            // btn_kvadrat_a
            // 
            this.btn_kvadrat_a.Location = new System.Drawing.Point(236, 154);
            this.btn_kvadrat_a.Name = "btn_kvadrat_a";
            this.btn_kvadrat_a.Size = new System.Drawing.Size(75, 23);
            this.btn_kvadrat_a.TabIndex = 16;
            this.btn_kvadrat_a.Text = "a*a :";
            this.btn_kvadrat_a.UseVisualStyleBackColor = true;
            this.btn_kvadrat_a.Click += new System.EventHandler(this.btn_kvadrat_a_Click);
            // 
            // txt_sin_a
            // 
            this.txt_sin_a.Location = new System.Drawing.Point(318, 37);
            this.txt_sin_a.Name = "txt_sin_a";
            this.txt_sin_a.Size = new System.Drawing.Size(100, 20);
            this.txt_sin_a.TabIndex = 17;
            this.txt_sin_a.TextChanged += new System.EventHandler(this.txt_sin_a_TextChanged);
            // 
            // txt_cos_a
            // 
            this.txt_cos_a.Location = new System.Drawing.Point(318, 67);
            this.txt_cos_a.Name = "txt_cos_a";
            this.txt_cos_a.Size = new System.Drawing.Size(100, 20);
            this.txt_cos_a.TabIndex = 18;
            this.txt_cos_a.TextChanged += new System.EventHandler(this.txt_cos_a_TextChanged);
            // 
            // txt_log_a
            // 
            this.txt_log_a.Location = new System.Drawing.Point(318, 97);
            this.txt_log_a.Name = "txt_log_a";
            this.txt_log_a.Size = new System.Drawing.Size(100, 20);
            this.txt_log_a.TabIndex = 19;
            this.txt_log_a.TextChanged += new System.EventHandler(this.txt_log_a_TextChanged);
            // 
            // txt_sqrt_a
            // 
            this.txt_sqrt_a.Location = new System.Drawing.Point(318, 126);
            this.txt_sqrt_a.Name = "txt_sqrt_a";
            this.txt_sqrt_a.Size = new System.Drawing.Size(100, 20);
            this.txt_sqrt_a.TabIndex = 20;
            this.txt_sqrt_a.TextChanged += new System.EventHandler(this.txt_sqrt_a_TextChanged);
            // 
            // txt_kvadrat_a
            // 
            this.txt_kvadrat_a.Location = new System.Drawing.Point(318, 157);
            this.txt_kvadrat_a.Name = "txt_kvadrat_a";
            this.txt_kvadrat_a.Size = new System.Drawing.Size(100, 20);
            this.txt_kvadrat_a.TabIndex = 21;
            this.txt_kvadrat_a.TextChanged += new System.EventHandler(this.txt_kvadrat_a_TextChanged);
            // 
            // btn_sin_b
            // 
            this.btn_sin_b.Location = new System.Drawing.Point(479, 34);
            this.btn_sin_b.Name = "btn_sin_b";
            this.btn_sin_b.Size = new System.Drawing.Size(75, 23);
            this.btn_sin_b.TabIndex = 22;
            this.btn_sin_b.Text = "sin b:";
            this.btn_sin_b.UseVisualStyleBackColor = true;
            this.btn_sin_b.Click += new System.EventHandler(this.btn_sin_b_Click);
            // 
            // btn_cos_b
            // 
            this.btn_cos_b.Location = new System.Drawing.Point(479, 63);
            this.btn_cos_b.Name = "btn_cos_b";
            this.btn_cos_b.Size = new System.Drawing.Size(75, 23);
            this.btn_cos_b.TabIndex = 23;
            this.btn_cos_b.Text = "cos b:";
            this.btn_cos_b.UseVisualStyleBackColor = true;
            this.btn_cos_b.Click += new System.EventHandler(this.btn_cos_b_Click);
            // 
            // btn_log_b
            // 
            this.btn_log_b.Location = new System.Drawing.Point(479, 94);
            this.btn_log_b.Name = "btn_log_b";
            this.btn_log_b.Size = new System.Drawing.Size(75, 23);
            this.btn_log_b.TabIndex = 24;
            this.btn_log_b.Text = "log b:";
            this.btn_log_b.UseVisualStyleBackColor = true;
            this.btn_log_b.Click += new System.EventHandler(this.btn_log_b_Click);
            // 
            // btn_sqrt_b
            // 
            this.btn_sqrt_b.Location = new System.Drawing.Point(479, 122);
            this.btn_sqrt_b.Name = "btn_sqrt_b";
            this.btn_sqrt_b.Size = new System.Drawing.Size(75, 23);
            this.btn_sqrt_b.TabIndex = 25;
            this.btn_sqrt_b.Text = "sqrt b:";
            this.btn_sqrt_b.UseVisualStyleBackColor = true;
            this.btn_sqrt_b.Click += new System.EventHandler(this.btn_sqrt_b_Click);
            // 
            // btn_kvadrat_b
            // 
            this.btn_kvadrat_b.Location = new System.Drawing.Point(479, 153);
            this.btn_kvadrat_b.Name = "btn_kvadrat_b";
            this.btn_kvadrat_b.Size = new System.Drawing.Size(75, 23);
            this.btn_kvadrat_b.TabIndex = 26;
            this.btn_kvadrat_b.Text = "b*b:";
            this.btn_kvadrat_b.UseVisualStyleBackColor = true;
            this.btn_kvadrat_b.Click += new System.EventHandler(this.btn_kvadrat_b_Click);
            // 
            // txt_sin_b
            // 
            this.txt_sin_b.Location = new System.Drawing.Point(561, 34);
            this.txt_sin_b.Name = "txt_sin_b";
            this.txt_sin_b.Size = new System.Drawing.Size(100, 20);
            this.txt_sin_b.TabIndex = 27;
            this.txt_sin_b.TextChanged += new System.EventHandler(this.txt_sin_b_TextChanged);
            // 
            // txt_cos_b
            // 
            this.txt_cos_b.Location = new System.Drawing.Point(561, 65);
            this.txt_cos_b.Name = "txt_cos_b";
            this.txt_cos_b.Size = new System.Drawing.Size(100, 20);
            this.txt_cos_b.TabIndex = 28;
            this.txt_cos_b.TextChanged += new System.EventHandler(this.txt_cos_b_TextChanged);
            // 
            // txt_log_b
            // 
            this.txt_log_b.Location = new System.Drawing.Point(561, 96);
            this.txt_log_b.Name = "txt_log_b";
            this.txt_log_b.Size = new System.Drawing.Size(100, 20);
            this.txt_log_b.TabIndex = 29;
            this.txt_log_b.TextChanged += new System.EventHandler(this.txt_log_b_TextChanged);
            // 
            // txt_sqrt_b
            // 
            this.txt_sqrt_b.Location = new System.Drawing.Point(561, 123);
            this.txt_sqrt_b.Name = "txt_sqrt_b";
            this.txt_sqrt_b.Size = new System.Drawing.Size(100, 20);
            this.txt_sqrt_b.TabIndex = 30;
            this.txt_sqrt_b.TextChanged += new System.EventHandler(this.txt_sqrt_b_TextChanged);
            // 
            // txt_kvadrat_b
            // 
            this.txt_kvadrat_b.Location = new System.Drawing.Point(561, 155);
            this.txt_kvadrat_b.Name = "txt_kvadrat_b";
            this.txt_kvadrat_b.Size = new System.Drawing.Size(100, 20);
            this.txt_kvadrat_b.TabIndex = 31;
            this.txt_kvadrat_b.TextChanged += new System.EventHandler(this.txt_kvadrat_b_TextChanged);
            // 
            // btn_izlaz
            // 
            this.btn_izlaz.Location = new System.Drawing.Point(699, 226);
            this.btn_izlaz.Name = "btn_izlaz";
            this.btn_izlaz.Size = new System.Drawing.Size(75, 23);
            this.btn_izlaz.TabIndex = 32;
            this.btn_izlaz.Text = "izlaz";
            this.btn_izlaz.UseVisualStyleBackColor = true;
            this.btn_izlaz.Click += new System.EventHandler(this.btn_izlaz_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(810, 261);
            this.Controls.Add(this.btn_izlaz);
            this.Controls.Add(this.txt_kvadrat_b);
            this.Controls.Add(this.txt_sqrt_b);
            this.Controls.Add(this.txt_log_b);
            this.Controls.Add(this.txt_cos_b);
            this.Controls.Add(this.txt_sin_b);
            this.Controls.Add(this.btn_kvadrat_b);
            this.Controls.Add(this.btn_sqrt_b);
            this.Controls.Add(this.btn_log_b);
            this.Controls.Add(this.btn_cos_b);
            this.Controls.Add(this.btn_sin_b);
            this.Controls.Add(this.txt_kvadrat_a);
            this.Controls.Add(this.txt_sqrt_a);
            this.Controls.Add(this.txt_log_a);
            this.Controls.Add(this.txt_cos_a);
            this.Controls.Add(this.txt_sin_a);
            this.Controls.Add(this.btn_kvadrat_a);
            this.Controls.Add(this.btn_sqrt_a);
            this.Controls.Add(this.btn_log_a);
            this.Controls.Add(this.btn_cos_a);
            this.Controls.Add(this.btn_sin_a);
            this.Controls.Add(this.txt_podijeli);
            this.Controls.Add(this.txt_pomnozi);
            this.Controls.Add(this.txt_oduzmi);
            this.Controls.Add(this.txt_zbroji);
            this.Controls.Add(this.btn_podijeli);
            this.Controls.Add(this.btn_pomnozi);
            this.Controls.Add(this.btn_oduzmi);
            this.Controls.Add(this.btn_zbroj);
            this.Controls.Add(this.txt_b);
            this.Controls.Add(this.txt_a);
            this.Controls.Add(this.lbl_b);
            this.Controls.Add(this.lbl_a);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_a;
        private System.Windows.Forms.Label lbl_b;
        private System.Windows.Forms.TextBox txt_a;
        private System.Windows.Forms.TextBox txt_b;
        private System.Windows.Forms.Button btn_zbroj;
        private System.Windows.Forms.Button btn_oduzmi;
        private System.Windows.Forms.Button btn_pomnozi;
        private System.Windows.Forms.Button btn_podijeli;
        private System.Windows.Forms.TextBox txt_zbroji;
        private System.Windows.Forms.TextBox txt_oduzmi;
        private System.Windows.Forms.TextBox txt_pomnozi;
        private System.Windows.Forms.TextBox txt_podijeli;
        private System.Windows.Forms.Button btn_sin_a;
        private System.Windows.Forms.Button btn_cos_a;
        private System.Windows.Forms.Button btn_log_a;
        private System.Windows.Forms.Button btn_sqrt_a;
        private System.Windows.Forms.Button btn_kvadrat_a;
        private System.Windows.Forms.TextBox txt_sin_a;
        private System.Windows.Forms.TextBox txt_cos_a;
        private System.Windows.Forms.TextBox txt_log_a;
        private System.Windows.Forms.TextBox txt_sqrt_a;
        private System.Windows.Forms.TextBox txt_kvadrat_a;
        private System.Windows.Forms.Button btn_sin_b;
        private System.Windows.Forms.Button btn_cos_b;
        private System.Windows.Forms.Button btn_log_b;
        private System.Windows.Forms.Button btn_sqrt_b;
        private System.Windows.Forms.Button btn_kvadrat_b;
        private System.Windows.Forms.TextBox txt_sin_b;
        private System.Windows.Forms.TextBox txt_cos_b;
        private System.Windows.Forms.TextBox txt_log_b;
        private System.Windows.Forms.TextBox txt_sqrt_b;
        private System.Windows.Forms.TextBox txt_kvadrat_b;
        private System.Windows.Forms.Button btn_izlaz;
    }
}

