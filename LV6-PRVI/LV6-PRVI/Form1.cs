﻿//Napravite aplikaciju znanstveni kalkulator koja će imati funkcionalnost
//znanstvenog kalkulatora, odnosno implementirati osnovne(+,-,*,/) i barem 5
//naprednih(sin, cos, log, sqrt...) operacija.


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV6_PRVI
{
    public partial class Form1 : Form
    {
        double a;
        double b;
        public Form1()
        {
            InitializeComponent();
        }

        private void lbl_a_Click(object sender, EventArgs e)
        {

        }

        private void txt_a_TextChanged(object sender, EventArgs e)
        {

        }

        private void lbl_b_Click(object sender, EventArgs e)
        {

        }

        private void txt_b_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void btn_zbroj_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txt_a.Text, out a))
                MessageBox.Show("pogresan unos prvog broja", "pogreska!");
            else if (!double.TryParse(txt_b.Text, out b))
                MessageBox.Show("pogresan unos drugog broja", "pogreska!");
            else
            {
                txt_zbroji.Text = (a + b).ToString();
            }
        }

        private void txt_zbroji_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_oduzmi_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txt_a.Text, out a))
                MessageBox.Show("pogresan unos prvog broja", "pogreska!");
            else if (!double.TryParse(txt_b.Text, out b))
                MessageBox.Show("pogresan unos drugog broja", "pogreska!");
            else
            {
                txt_oduzmi.Text = (a - b).ToString();
            }
        }

        private void txt_oduzmi_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_pomnozi_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txt_a.Text, out a))
                MessageBox.Show("pogresan unos prvog broja", "pogreska!");
            else if (!double.TryParse(txt_b.Text, out b))
                MessageBox.Show("pogresan unos drugog broja", "pogreska!");
            else
            {
                txt_pomnozi.Text = (a * b).ToString();
            }
        }

        private void txt_pomnozi_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_podijeli_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txt_a.Text, out a))
                MessageBox.Show("pogresan unos prvog broja", "pogreska!");
            else if (!double.TryParse(txt_b.Text, out b) || b == 0)
                MessageBox.Show("pogresan unos drugog broja", "pogreska!");
            else
            {
                txt_podijeli.Text = (a / b).ToString();
            }
        }

        private void txt_podijeli_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_sin_a_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txt_a.Text, out a))
                MessageBox.Show("pogresan unos prvog broja", "pogreska!");
            else
            {
                txt_sin_a.Text = (System.Math.Sin(a)).ToString();
            }
        }

        private void txt_sin_a_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_cos_a_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txt_a.Text, out a))
                MessageBox.Show("pogresan unos prvog broja", "pogreska!");
            else
            {
                txt_cos_a.Text = (System.Math.Cos(a)).ToString();
            }
        }

        private void txt_cos_a_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_log_a_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txt_a.Text, out a) || a <= 0)
                MessageBox.Show("pogresan unos prvog broja", "pogreska!");
            else
            {
                txt_log_a.Text = (System.Math.Log10(a)).ToString();
            }
        }

        private void txt_log_a_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_sqrt_a_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txt_a.Text, out a) || a < 0)
                MessageBox.Show("pogresan unos prvog broja", "pogreska!");
            else
            {
                txt_sqrt_a.Text = (System.Math.Sqrt(a)).ToString();
            }
        }

        private void txt_sqrt_a_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_kvadrat_a_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txt_a.Text, out a))
                MessageBox.Show("pogresan unos prvog broja", "pogreska!");
            else
            {
                txt_kvadrat_a.Text = (a * a).ToString();
            }
        }

        private void txt_kvadrat_a_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_sin_b_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txt_b.Text, out b))
                MessageBox.Show("pogresan unos drugog broja", "pogreska!");
            else
            {
                txt_sin_b.Text = (System.Math.Sin(b)).ToString();
            }
        }

        private void txt_sin_b_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_cos_b_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txt_b.Text, out b))
                MessageBox.Show("pogresan unos drugog broja", "pogreska!");
            else
            {
                txt_cos_b.Text = (System.Math.Cos(b)).ToString();
            }
        }

        private void txt_cos_b_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_log_b_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txt_b.Text, out b) || b <= 0)
                MessageBox.Show("pogresan unos drugog broja", "pogreska!");
            else
            {
                txt_log_b.Text = (System.Math.Log10(b)).ToString();
            }
        }

        private void txt_log_b_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_sqrt_b_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txt_b.Text, out b) || b < 0)
                MessageBox.Show("pogresan unos drugog broja", "pogreska!");
            else
            {
                txt_sqrt_b.Text = (System.Math.Sqrt(b)).ToString();
            }
        }

        private void txt_sqrt_b_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_kvadrat_b_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txt_b.Text, out b))
                MessageBox.Show("pogresan unos drugog broja", "pogreska!");
            else
            {
                txt_kvadrat_b.Text = (b * b).ToString();
            }
        }

        private void txt_kvadrat_b_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_izlaz_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
